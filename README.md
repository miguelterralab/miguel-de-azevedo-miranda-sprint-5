# Projeto: TerraLAB - Automatização de Coleta de Dados da Kabum

## Atividade Prática - Sprint 4

**Autor:** Miguel De Azevedo Miranda

### Contexto do Projeto

Este projeto para o TerraLAB é uma solução em Python desenvolvida para automatizar a coleta de informações detalhadas de celulares no site da Kabum. O objetivo é gerar um arquivo que permita aos clientes realizar análises aprofundadas de maneira fácil e eficiente.

### Extensões Utilizadas

No desenvolvimento desta solução, foram utilizadas as seguintes extensões:

- Selenium
- BeautifulSoup
- Pandas
- Math
- Os
- Regular Expressions (re)

### Como Utilizar

Para utilizar o código, siga as instruções abaixo:

1. **Instalação de Dependências:**
   - Certifique-se de ter o Python instalado em seu ambiente.
   - Instale as dependências necessárias executando o comando:
     ```
     pip install selenium beautifulsoup4 pandas
     ```

2. **Execução do Código:**
   - Abra o código em seu ambiente de desenvolvimento.
   - Pressione F5 para executar o programa.

3. **Aguarde a Coleta de Dados:**
   - O programa iniciará a automação da coleta de informações do site da Kabum.
   - Aguarde entre 1 minuto e 10 segundos e 1 minuto e 30 segundos para a conclusão.

4. **Arquivo Resultante:**
   - Após o tempo de espera, o programa criará um arquivo CSV chamado `preços_smartphone.csv`.
   - Este arquivo conterá informações sobre os celulares disponíveis no site da Kabum, incluindo marca e preço.

5. **Análises Adicionais:**
   - Os dados no arquivo CSV estarão prontos para análises mais aprofundadas por parte dos usuários.

### Observações Importantes

- Certifique-se de ter o WebDriver adequado para o seu navegador instalado e configurado.
- Este código usa o Chrome como navegador, então você pode precisar baixar o ChromeDriver [aqui](https://sites.google.com/chromium.org/driver/) e ajustar o caminho no código.

