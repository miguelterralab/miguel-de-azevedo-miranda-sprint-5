from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import pandas as pd 
import math
import os
import re

# Configurando as opções do Chrome para não abrir ao rodar o programa
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-gpu")

# Redirecionando a saída padrão e de erro para o nada
os.environ['DISPLAY'] = ":0.0"
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('log-level=3')

driver = webdriver.Chrome(options=chrome_options)

url = 'https://www.kabum.com.br/celular-smartphone/smartphones'
driver.get(url)

soup = BeautifulSoup(driver.page_source, 'html.parser')
qtd_itens = soup.find('div', id='listingCount').get_text().strip()

index = qtd_itens.find(' ')
qtd = qtd_itens[:index]

ultima_pagina = math.ceil(int(qtd) / 100)

dic_produtos = {'marca': [], 'preco': []}

for i in range(1, ultima_pagina + 1):
    url_page = f'https://www.kabum.com.br/celular-smartphone/smartphones?page_number={i}&page_size=100&facet_filters=&sort=-price'
    driver.get(url_page)

    soup = BeautifulSoup(driver.page_source, 'html.parser')
    produtos = soup.find_all('div', class_=re.compile('productCard'))

    for produto in produtos:
        marca = produto.find('span', class_=re.compile('nameCard')).get_text().strip()
        preco = produto.find('span', class_=re.compile('priceCard')).get_text().strip()


        print(marca, preco)

        dic_produtos['marca'].append(marca)
        dic_produtos['preco'].append(preco)

    print(url_page)
driver.quit()

df = pd.DataFrame(dic_produtos)
df.to_csv('C:\\Users\\migue\\OneDrive\\Área de Trabalho\\Webscrap\\preços_smartphone.csv', encoding='utf-8', sep=';')
